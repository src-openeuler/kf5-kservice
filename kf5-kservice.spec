%undefine __cmake_in_source_build
%global vendor %{?_vendor:%{_vendor}}%{!?_vendor:openEuler}
%global framework kservice

Name:           kf5-%{framework}
Summary:        KDE Frameworks 5 Tier 3 solution for advanced plugin and service introspection
Version:        5.116.0
Release:        1

# mixture of LGPLv2 and LGPLv2+ (mostly the latter)
License:        CC0-1.0 AND GPL-2.0-only AND GPL-2.0-or-later AND GPL-3.0-only AND LGPL-2.0-only AND LGPL-2.0-or-later AND LGPL-2.1-only AND LGPL-3.0-only AND (GPL-2.0-only OR GPL-3.0-only) AND (LGPL-2.1-only OR LGPL-3.0-only)
URL:            https://invent.kde.org/frameworks/%{framework}

%global majmin %majmin_ver_kf5
%global stable %stable_kf5

Source0:        http://download.kde.org/%{stable}/frameworks/%{majmin}/%{framework}-%{version}.tar.xz

## downstream patches
# Fedora customizations to the menu categories
# adds the Administration menu from redhat-menus which equals System + Settings
# This prevents the stuff getting listed twice, under both System and Settings.
Patch100:  kservice-5.15.0-xdg-menu.patch

# kbuildsycoca5 always gives:
# kf5.kservice.sycoca: Parse error in  "$HOME/.config/menus/applications-merged/xdg-desktop-menu-dummy.menu" , line  1 , col  1 :  "unexpected end of file"
# hide that by default, make it qCDebug instead (of qCWarning)
Patch101:  kservice-5.17.0-vfolder_spam.patch

BuildRequires:  extra-cmake-modules >= %{majmin}
BuildRequires:  kf5-kconfig-devel >= %{majmin}
BuildRequires:  kf5-kcoreaddons-devel >= %{majmin}
BuildRequires:  kf5-kcrash-devel >= %{majmin}
BuildRequires:  kf5-kdbusaddons-devel >= %{majmin}
BuildRequires:  kf5-kdoctools-devel >= %{majmin}
BuildRequires:  kf5-ki18n-devel >= %{majmin}
BuildRequires:  kf5-rpm-macros
BuildRequires:  qt5-qtbase-devel

BuildRequires:  flex
BuildRequires:  bison

# for the Administration category
Requires:       %{vendor}-menus

%description
KDE Frameworks 5 Tier 3 solution for advanced plugin and service
introspection.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       kf5-kconfig-devel >= %{majmin}
Requires:       kf5-kcoreaddons-devel >= %{majmin}
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{version} -p1


%build
%{cmake_kf5}

%cmake_build


%install
%cmake_install

%find_lang %{name} --all-name --with-man

mv %{buildroot}%{_kf5_sysconfdir}/xdg/menus/applications.menu \
   %{buildroot}%{_kf5_sysconfdir}/xdg/menus/kf5-applications.menu

mkdir -p %{buildroot}%{_kf5_datadir}/kservices5
mkdir -p %{buildroot}%{_kf5_datadir}/kservicetypes5


%ldconfig_scriptlets

%files -f %{name}.lang
%doc README.md
%license LICENSES/*.txt
# this is not a config file, despite rpmlint complaining otherwise -- rex
%{_kf5_sysconfdir}/xdg/menus/kf5-applications.menu
%{_kf5_datadir}/qlogging-categories5/%{framework}.*
%{_kf5_bindir}/kbuildsycoca5
%{_kf5_libdir}/libKF5Service.so.5*
%{_kf5_datadir}/kservicetypes5/
%{_kf5_datadir}/kservices5/
%{_kf5_mandir}/man8/*.8*

%files devel
%{_kf5_includedir}/KService/
%{_kf5_libdir}/libKF5Service.so
%{_kf5_libdir}/cmake/KF5Service/
%{_kf5_archdatadir}/mkspecs/modules/qt_KService.pri


%changelog
* Wed Jan 22 2025 peijiankang <peijiankang@kylinos.cn> - 5.116.0-1
- update verison to 5.116.0

* Thu Nov 21 2024 tangjie02 <tangjie02@kylinsec.com.cn> - 5.115.0-2
- adapt to the new CMake macros to fix build failure

* Mon Mar 04 2024 peijiankang <peijiankang@kylinos.cn> - 5.115.0-1
- update verison to 5.115.0

* Tue Jan 02 2024 wangqia <wangqia@uniontech.com> - 5.113.0-1
- Update to upstream version 5.113.0

* Thu Aug 03 2023 wangqia <wangqia@uniontech.com> - 5.108.0-1
- Update to upstream version 5.108.0

* Tue Apr 11 2023 panchenbo <panchenbo@uniontech.com> - 5.100.0-2
- modify openEuler to vendor

* Tue Dec 13 2022 lijian <lijian2@kylinos.cn> - 5.100.0-1
- update to upstream version 5.100.0

* Mon Sep 05 2022 liweiganga <liweiganga@uniontech.com> - 5.97.0-1
- update to upstream version 5.97.0

* Tue Jul 05 2022 peijiankang <peijiankang@kylinos.cn> - 5.95.0-1
- update to upstream version 5.95.0

* Sat Feb 12 2022 peijiankang <peijiankang@kylinos.cn> - 5.90.0-1
- update to upstream version 5.90.0

* Sun Jan 16 2022 peijiankang <peijiankang@kylinos.cn> - 5.88.0-1
- update to upstream version 5.88.0

* Wed Jul 21 2021 weidong <weidong@uniontech.com> - 5.55.0-2
- Update requires

* Mon Aug 17 2020 yeqinglong <yeqinglong@uniontech.com> - 5.55.0-1
- Initial release for OpenEuler

